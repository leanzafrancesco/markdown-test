# YITH Plugin Framework Handbook

1. [Including Plugin Framework](#including-plugin-framework)
    * [Action links](#action-links)
    * [Row meta](#row-meta)
1. [Metabox](#metabox)
1. [Custom Post Types](#custom-post-types)

## Including Plugin Framework

### Action links
### Row meta

## Metabox

## Custom Post Types

## Options Panel
To show the plugin settings, you can use the Options panels. 
You can choose between:
* **YIT Panel**: you should use it only if your plugin doesn't require WooCommerce
* **YIT Panel WooCommerce** (preferred): you can use it only if WooCommerce is enabled

Let's take a look how to create a Panel

```php
<?php
function this_is_a_test(){
    echo "This is a test";
}
```

```javascript
if (window.name=='' || (window.name.indexOf('wicket') > -1 && window.name!='wicket-wicket:default')) { 
window.location="register/wicket:pageMapName/wicket-0" + (window.location.hash != null ? window.location.hash : ""); 
}
```

### YIT Panel
### YIT Panel WooCommerce
### Custom Fields 
### Custom Tab

## Gutenberg e shortcode

## Privacy